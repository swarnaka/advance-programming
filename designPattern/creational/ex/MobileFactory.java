package designPattern.creational.ex;

public class MobileFactory {
	
	public static Mobile buildMobile(MobileType model) {
		
		Mobile mobile = null;
		
		switch(model) {
		
		case IPHONE:
			mobile =new IPhone();
			break;
			
		case SAMSUNG:
			mobile =new Samsung();
			break;
			
		case VIVO:
			mobile =new Vivo();
			break;
			
		default:
			// throw some exception
			break;
				
		}
				
		return mobile;
		
	}


}
