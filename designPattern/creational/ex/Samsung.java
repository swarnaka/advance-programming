package designPattern.creational.ex;

public class Samsung extends Mobile {
	
	MobileColor color=MobileColor.WHITE;

	public Samsung() {
		super(MobileType.SAMSUNG);
		construct();
	}
	
	public Samsung(MobileColor color) {
		super(MobileType.SAMSUNG);
		this.color=color;
		construct();
	}
	
	

	@Override
	protected void construct() {
		System.out.println("Introducing " + color + " Samsung");
		
	}

}
