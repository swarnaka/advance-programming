package designPattern.creational.ex;

public class IPhone extends Mobile{
	
	MobileColor color=MobileColor.BLACK;

	public IPhone() {
		super(MobileType.IPHONE);
		construct();
	}
	
	
	public IPhone(MobileColor color) {
		super(MobileType.IPHONE);
		this.color=color;
		construct();
	}
	

	@Override
	protected void construct() {
		System.out.println("Introducing " + color + "  IPhone");
		
	}
	protected void sprayPaint() {
		// TODO Auto-generated method stub
		
	}


}
