package designPattern.creational.ex;


public class MobileColorFactory {
	
public static Mobile buildMobile(MobileType model, MobileColor color ) {
		
		Mobile mobile = null;
		
		switch(model) {
		
		case IPHONE:
			mobile =new IPhone();
			break;
			
		case SAMSUNG:
			mobile =new Samsung();
			break;
			
		case VIVO:
			mobile =new Vivo();
			break;
			
		default:
			// throw some exception
			break;
				
		}
				
		return mobile;
		
	}




}
