package designPattern.creational.ex;

public class Vivo extends Mobile {
	
	MobileColor color=MobileColor.BLUE;

	public Vivo() {
		super(MobileType.VIVO);
		construct();
		
	}
	
	public Vivo(MobileColor color) {
		super(MobileType.VIVO);
		this.color=color;
		construct();
	}

	@Override
	protected void construct() {
		System.out.println("Introducing " + color + " Vivo");
		
	}

}
