package designPattern.creational.ex;

public class MobileTestFactory {
	
	public static void main(String[] args) {
		
		MobileFactory.buildMobile(MobileType.IPHONE);
		
		MobileColorFactory.buildMobile(MobileType.SAMSUNG, MobileColor.WHITE);
		
	}

}
