package designPattern.creational.ex;



public abstract class Mobile {
	
	private MobileType model;
	
protected abstract void construct();
	
	public Mobile(MobileType model) {
		this.model=model;
		arrangeParts();
				
	}

	private void arrangeParts() {
		// TODO Auto-generated method stub
		System.out.println("Arranging Parts for " + model);
		
	}
	
	public MobileType getModel() {
		return model;
		
		
	}
	
	public void setModel() {
		this.model=model;
		
		
	}

	

}
