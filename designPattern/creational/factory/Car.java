package designPattern.creational.factory;

public abstract class Car {
	
	private CarType model;
	
	protected abstract void construct();
	
	public Car(CarType model) {
		this.model=model;
		arrangeParts();
				
	}

	private void arrangeParts() {
		// TODO Auto-generated method stub
		System.out.println("Arranging Parts for " + model);
		
	}
	
	public CarType getModel() {
		return model;
		
		
	}
	
	public void setModel() {
		this.model=model;
		
		
	}

	
}
