package designPattern.creational.factory;

public class SmallCar extends Car {
	
	public SmallCar() {
		super(CarType.SMALL);
		construct();
	}


	@Override
	protected void construct() {
		System.out.println("Buildgin small car");
		
	}


}
