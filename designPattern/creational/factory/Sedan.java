package designPattern.creational.factory;

public class Sedan extends Car{
	
	
	public Sedan(CarType model) {
		super(CarType.SEDAN);
		construct();
		
	}
	
	@Override
	protected void construct() {
		System.out.println("Buildgin sedan car");
		
	}

	


}
