package designPattern.creational.factory;

public class TestCarFactory {
	
public static void main(String[] args) {
		
		CarFactory.buildCar(CarType.LUXURY);
	}

}
