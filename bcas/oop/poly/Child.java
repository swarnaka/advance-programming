package bcas.oop.poly;

public class Child extends Parent{
	
	public void printMessage() {
		System.out.println("Hi welcome from child");
		
	}
	
	
	public void printMessage(String msg) {
		System.out.println("Hi welcome + msg");
		
	}

}
