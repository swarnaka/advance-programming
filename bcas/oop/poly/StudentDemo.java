package bcas.oop.poly;

public class StudentDemo {
	
	public static void main(String[]args) {
		
		Student swarna =new Student("Swarna");
		swarna.printDetails();
		
		Student denci =new Student("Deniya","Jaffna");
		denci.printDetails();
		
		
		Student jens =new Student("Jensan","Jaffna",25,'A');
		jens.printDetails();
		
		
		denci.setAddress("Colombo");
		
		
	}

}
