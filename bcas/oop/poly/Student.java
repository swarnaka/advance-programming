package bcas.oop.poly;

//For Constructor Overloading

public class Student {
	
	private String name;
	private String address="-";
	private int age;
	private char grade='-';
	
	public Student() {
		this.name=name;		
	}
	
	
	public Student(String name) {
		this.name=name;		
	}
	
	public Student(String name, String address) {
		this.name=name;	
		this.address=address;
	}
	
	public Student(String name, String address, int age) {
		this.name=name;	
		this.address=address;
		
	}
	
	public void setAddress(String address) {
		
		this.address=address;
	}
	
	public Student(String name, String address, int age, char grade) {
		this.name=name;	
		this.address=address;
		this.age=age;
		this.grade=grade;
		
	}

	public void printDetails() {
		System.out.println("Name :" +this.name + "\nAddress :" + this.address 
				+ "\nAge :" + this.age + "\nGrade :" + this.grade);
		System.out.println("------------");
		
		
	}
}
