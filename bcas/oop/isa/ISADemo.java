package bcas.oop.isa;

public class ISADemo {
	public static void main(String[]args) {
		
		Animal animal=new Animal();
		System.out.println("-----------------");
		Mammal mammal =new Mammal();
		System.out.println("-----------------");
		
		Cat cat=new Cat();
		
		Reptile rep=new Reptile();
		
		boolean status =mammal.getClass().isInstance(cat);
		System.out.println(status);
		
		 status =rep.getClass().isInstance(cat);
		System.out.println(status);
		
		System.out.println(animal.getClass().isInstance(cat));
		System.out.println(animal.getClass().isInstance(rep));
	}

}
