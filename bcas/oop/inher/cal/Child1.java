package bcas.oop.inher.cal;

public class Child1 extends Parent1 {
	
double num1, num2;
	
	public Child1(double num1, double num2) {
		super(num1, num2);
	}
	
	
	public double multi(double num1, double num2) {
		return num1 * num2;
		
	}
	
	public double div(double num1, double num2) {
		return num1 / num2;
		
	}
	

}
