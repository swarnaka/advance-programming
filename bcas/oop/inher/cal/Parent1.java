package bcas.oop.inher.cal;

public class Parent1 {
	
	
	double num1, num2;
	
	public Parent1(double num1, double num2) {
		this.num1=num1;
		this.num2=num2;
		System.out.println(add());
		System.out.println(sub());
	}
	
		
	protected double add() {
		return num1 + num2;
		
	}
	
	protected double sub() {
		return num1 - num2;
		
	}
}
