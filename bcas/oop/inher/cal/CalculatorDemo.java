package bcas.oop.inher.cal;

public class CalculatorDemo {
	public static void main(String[]args) {
		AdvancedCalculator  calculator = new AdvancedCalculator ();
		System.out.println(calculator.addNum(25, 35));
		System.out.println(calculator.subNum(68, 32));
		System.out.println(calculator.multNum(45, 25));
		System.out.println(calculator.divNum(120, 30));
		System.out.println(calculator.findSin(30));
		System.out.println(calculator.findCos(45));
		System.out.println(calculator.findTan(90));
		
		
		
	}

}
