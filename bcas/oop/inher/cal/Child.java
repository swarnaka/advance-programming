package bcas.oop.inher.cal;

public class Child extends Parent {
	
	public void display() {
		System.out.println(super.address);
		super.message();
	}
	
	public static void main(String[]args) {
		Child child = new Child();
		child.display();
	}
	
	

}
