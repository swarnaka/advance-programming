package bcas.oop.inter;

public class Triangle1 implements Shape{
	
	double base=10;
	double side1 =7;
	double side2=3;
	double height=5;
	String color ="Green";
	boolean isfilled=true;

	@Override
	public double getArea() {
		
		return  (base *height)/2;
	}

	@Override
	public double getPerimeter() {
		
		return side1+side2+base;
	}

	@Override
	public boolean isFilled() {
		
		return isfilled;
	}

	@Override
	public String getColor() {
		
		return color;
	}


}
