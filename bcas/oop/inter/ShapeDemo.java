package bcas.oop.inter;

public class ShapeDemo {
	
	public static void main(String[] args) {
		Square sq=new Square();
		System.out.println(sq.getArea());
		System.out.println(sq.getPerimeter());
		System.out.println(sq.isFilled());
		System.out.println(sq.getColor());
		
		System.out.println("...............................................");
		
		Rectangle1 re=new Rectangle1();
		System.out.println(re.getArea());
		System.out.println(re.getPerimeter());
		System.out.println(re.isFilled());
		System.out.println(re.getColor());
		System.out.println("...............................................");
		
		Triangle1 te=new Triangle1();
		System.out.println(te.getArea());
		System.out.println(te.getPerimeter());
		System.out.println(te.isFilled());
		System.out.println(te.getColor());
		System.out.println("...............................................");
		
		
		
		
	}


}
