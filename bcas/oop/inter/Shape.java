package bcas.oop.inter;

public interface Shape {
	
	public double getArea();
	
	public double getPerimeter();
	
	public boolean isFilled();
	
	public String getColor();

}
