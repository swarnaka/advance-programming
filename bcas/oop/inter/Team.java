package bcas.oop.inter;

public class Team implements BasketBallTeam,FootballTeam{
	
	private int basketBallTeamSize;
	private int footBallTeamSize;
	
	@Override
	public void setBasketBallTeamSize() {
		
		basketBallTeamSize=10;
	}
	@Override
	public void printBasketBallTeamName() {
		
		System.out.println("Team Name: Lotus,"+ "Team size:" +this.basketBallTeamSize);
	}

	@Override
	public void setFootballTeamSize() {
		footBallTeamSize=15;
		
	}

	@Override
	public void printFootballTeamName() {
		System.out.println("Team Name:Beta,"+ "Team size:" +this.footBallTeamSize);
	}



}
