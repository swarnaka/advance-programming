package bcas.oop.inter;

public class Rectangle1 implements Shape {

	double width=5;
	double base=12;
	double side=7;
	double height=6;
	String color="Blue";
	boolean isFilled =true;
	
	
    @Override     
        public double getArea() {                  
    	
    	return width*height;     
    	
    }

    @Override     
    public double getPerimeter() {                  
    	
    	return 2*(width*height);    
    	
    }

    @Override     
    public boolean isFilled() {                 
    	
    	return false;    
    	
    }

    @Override    
    public String getColor() {                  
    	
    	return color;     
    	
    }

}
