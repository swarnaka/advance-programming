package bcas.oop.inter;

public class Square implements Shape{
	
	double width =10;
	String color="Pink";
	boolean isFilled =true;
	


	@Override
	public double getArea() {
		
		return width * width;
	}
	

	
	@Override
	public double getPerimeter() {
		
		return 4*width;
	}

	@Override
	public boolean isFilled() {
		
		return false;
	}

	@Override
	public String getColor () {
		
		return color;
	}
}
