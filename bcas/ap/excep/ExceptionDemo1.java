package bcas.ap.excep;

public class ExceptionDemo1 {
	
	public static void main(String[]args) {
	
	int num[] = new int[3];
	
	num[1]=10;
	try {
		num[3]=30;
		
	}catch (ArrayIndexOutOfBoundsException NullPointerException e) {
		
		System.err.println(e.toString());
	}
	System.err.println("######################");
	
	String campus="BCAS";
	
	try {
		System.out.println(campus.length());
		
	}catch (Exception e) {
		
		System.err.println(e.getMessage());
	}
	
	
	}

}
