package bcas.ap.excep;

public class Account {
	private double balance;
	private String accNumber;
	
	public Account (String accNumber) {
		this.accNumber=accNumber;
		
	}
	public void deposite (double amount) throws MaxDepositException,MiniDepositException,InSufficientWithdrawException{
		if(amount <100) {
			throw new MiniDepositException();
		}
		else if(amount>100000) {
			throw new MiniDepositException();
		}
		else {
			balance +=amount ;
		}
	}
	public void withdraw (double Wamount) throws InSufficientWithdrawException{
		if(Wamount <balance) {
			balance -=Wamount ;
		}
		else {
			throw new InSufficientWithdrawException();
		}
		
		
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public String getAccNumber() {
		return accNumber;
	}
	public void setAccNumber(String accNumber) {
		this.accNumber = accNumber;
	}
	
}
