package com.bcas.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.cj.jdbc.DatabaseMetaData;


public class JDBCSingleton {
	
	private static String url ="jdbc:mysql://localhost:3300/";
	private static String username ="root";
	private static String password ="Swarnaka0408";
	private static String database="csd14";
	
	
	private static JDBCSingleton jdbc=null;
	
	
	private JDBCSingleton() {
		
		
	}
	
	
	public static JDBCSingleton getInstance() {
		
		if( jdbc == null) {
			jdbc = new JDBCSingleton();
			
			
		}
		
		return jdbc;
		
			
	}
	
	public static Connection getConnection() throws ClassNotFoundException , SQLException {
		Connection connection = null;
	//	Class.forName("com.mysql.jdbc.Driver");
		connection=DriverManager.getConnection(DBProperties.URL.value+ DBProperties.DATABASE.value ,DBProperties.USERNAME.value ,DBProperties.PASSWORD.value);
		return connection;
		
	}
	
	// to insert the table record into the database
		

		public void insert(int Id , String Name, String Address) throws SQLException, ClassNotFoundException {
			
			Connection connection =null;
			PreparedStatement statement=null;
			
			connection=getConnection();
			statement=connection.prepareStatement("INSERT INTO student(Id,Name,Address)VALUES(?,?,?)");
			statement.setInt(1, Id);
			statement.setString(2, Name);
			statement.setString(3, Address);
			
			int rowCount=statement.executeUpdate();
			if(statement != null) {
				statement.close();
			}
			
			if(connection != null) {
				connection.close();
			}
			
			
		}
			
			
		
		public void view(String Name) throws SQLException, ClassNotFoundException {
			
			Connection connection = null;
			ResultSet resultset=null;
			
			
			connection = getConnection();
			resultset=connection.createStatement().executeQuery("SELECT * FROM student WHERE Name='" + Name +"'");

			while(resultset.next()) {
				System.out.println("Student Name =" + resultset.getString(2)+"\n"  +"Student Address =" + resultset.getString(3));
				
			}
			
			if(connection != null) {
				connection.close();
			}
		}
		
		public void viewAll() throws SQLException, ClassNotFoundException {
			
			Connection connection = null;
			ResultSet resultset=null;
			
			
			connection = getConnection();
			resultset=connection.createStatement().executeQuery("SELECT * FROM student");

			while(resultset.next()) {
				System.out.println("Student Name =" + resultset.getString(2)+"\n"  +"Student Address =" + resultset.getString(3));
				
			}
			
			if(connection != null) {
				connection.close();
			}
		}



		
		public void delete(String Name) throws SQLException, ClassNotFoundException {
			
			Connection connection = null;
					
			connection = getConnection();
			int rowCount=connection.prepareStatement("DELETE FROM student WHERE Name='" + Name + "'").executeUpdate();

					
			if(connection != null) {
				connection.close();
			}
			System.out.println("Row count-" + rowCount);
		}
		
		
		
		public void update(int Id , String Name, String Address) throws SQLException, ClassNotFoundException {
			
			Connection connection = null;
					
			connection = getConnection();
			int count=connection.prepareStatement("UPDATE student SET Address='" + Address +"'WHERE Name='" +Name+ "'").executeUpdate();

					
			if(connection != null) {
				connection.close();
			}
			System.out.println("Row count-" + count);
		}

}
