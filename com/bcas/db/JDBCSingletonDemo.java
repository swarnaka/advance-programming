package com.bcas.db;

import java.sql.SQLException;


public class JDBCSingletonDemo {
	
	public static void main(String[] args) throws SQLException {

		JDBCSingleton db=JDBCSingleton.getInstance();
		
		try {
			db.insert(5, "Paree", "Kodikamam");
			System.out.println("Data has been Inserted successfully!!!");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		
		}
		
		System.out.println("----------------------------------------------------------------");
		
		try {
			db.view("Shobi");
		} catch (ClassNotFoundException | SQLException e) {
			
			e.printStackTrace();
		}
		
		
		System.out.println("---------------------------------------------------------------");
		
		
		try {
			db.viewAll();
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		System.out.println("--------------------------------------------------------------");
		
		try {
			db.delete("Swarna");
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		
		System.out.println("--------------------------------------------------------------");
		
		
		try {
			db.update(3, "Kobi", "Jaffna");
		} catch (ClassNotFoundException  | SQLException e) {
			e.printStackTrace();
		}
		
		System.out.println("--------------------------------------------------------------");
		
		try {
			db.viewAll();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}
}
